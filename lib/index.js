'use strict';

const Package = require('../package.json');
const Routes = require('./controllers');

exports.register = function (server, options, next) {

    server.route(Routes);

    next();
};

exports.register.attributes = {
    name: Package.name,
    version: Package.version
};
