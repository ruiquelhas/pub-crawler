'use strict';

module.exports = [{
    method: 'DELETE',
    path: '/',
    async handler(request, reply) {

        const db = request.server.app.db;

        try {
            await db.clear();
        }
        catch (err) {
            return reply(err);
        }

        reply();
    }
}, {
    method: 'GET',
    path: '/',
    handler(request, reply) {

        reply().redirect('/pubs');
    }
}];
