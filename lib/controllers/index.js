'use strict';

const Pubs = require('./pubs');
const Root = require('./root');

module.exports = Pubs.concat(Root);
