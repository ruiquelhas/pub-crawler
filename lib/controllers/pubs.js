'use strict';

const Joi = require('joi');
const Boom = require('boom');
const Qs = require('qs');

module.exports = [{
    method: 'GET',
    path: '/pubs',
    config: {
        async handler(request, reply) {

            const pubs = [];
            const db = request.server.app.db;
            const query = Qs.parse(request.url.query);
            const limit = request.query.size;
            const skip = (request.query.page - 1) * limit;
            const criteria = db.prepare(query) || 'true';

            try {
                await db
                    .getSchema()
                    .getCollection('pubs')
                    .find(criteria)
                    .limit(limit, skip)
                    .execute((pub) => pub && pubs.push(pub));
            }
            catch (err) {
                return reply(err);
            }

            reply(pubs);
        },
        validate: {
            query: Joi.object({
                page: Joi.number().default(1).min(1).description('the page number'),
                size: Joi.number().default(5).min(1).description('the size of each page')
            }).options({ allowUnknown: true })
        }
    }
}, {
    method: 'GET',
    path: '/pubs/{id}',
    config: {
        async handler(request, reply) {

            const db = request.server.app.db;
            const pubs = [];

            try {
                await db
                    .getSchema()
                    .getCollection('pubs')
                    .find(`$._id == "${request.params.id}"`)
                    .execute((pub) => pub && pubs.push(pub));
            }
            catch (err) {
                return reply(err);
            }

            if (!pubs.length) {
                return reply(Boom.notFound());
            }

            reply(pubs[0]);
        },
        validate: {
            params: {
                id: Joi.number().description('the pub id').tags(['pub'])
            }
        }
    }
}];
