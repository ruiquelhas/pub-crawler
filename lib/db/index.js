'use strict';

const Flat = require('flat');
const X = require('@mysql/xdevapi');

module.exports = (state) => {

    state = Object.assign({}, state);

    const bootstrap = async function () {

        const schema = state.session.getSchema(state.connection.schema);
        await schema.createCollection('pubs', { ReuseExistingObject: true });
    };

    const comparify = function (data) {

        if (!data.length) {
            return 'LIKE "%%"';
        }

        if (data[0] !== '%' && data[data.length] !== '%') {
            return `== "${data}"`;
        }

        return `LIKE "${data}"`;
    };

    return {
        connection(config) {

            state.connection = config;

            return this;
        },

        async clear() {

            if (!state.session) {
                state.session = await X.getSession(state.connection);
            }

            const schema = state.session.getSchema(state.connection.schema);
            const collections = await schema.getCollections();

            for (const collection of Object.keys(collections)) {
                await collections[collection].remove('true').execute();
            }
        },

        async disconnect() {

            return await state.session.close();
        },

        getSchema: () => state.schema,

        async reset() {

            if (!state.session) {
                state.session = await X.getSession(state.connection);
            }

            const schema = state.session.getSchema(state.connection.schema).getName();
            await state.session.dropSchema(schema);
        },

        async start() {

            try {
                if (!state.session) {
                    state.session = await X.getSession(state.connection);
                }

                state.schema = await state.session.createSchema(state.connection.schema);

                return await bootstrap();
            }
            catch (err) {

                if (state.session && err.info && err.info.code === 1007) {
                    state.schema = state.session.getSchema(state.connection.schema);
                    return await bootstrap();
                }

                throw err;
            }
        },

        prepare(criteria, options) {

            options = Object.assign({ aggregator: '&&', omit: ['page', 'size'] }, options);

            const flat = Flat.flatten(criteria);
            const keys = Object.keys(flat).filter((key) => options.omit.indexOf(key) === -1);

            return keys.reduce((result, key, index) => {

                const compExpr = result
                    .concat('(')
                    .concat(`$.${key}`)
                    .concat(comparify(flat[key]))
                    .concat(')');

                if (index === keys.length - 1) {
                    return compExpr;
                }

                return result.concat(compExpr).concat(options.aggregator);
            }, []).join(' ');
        }
    };
};
