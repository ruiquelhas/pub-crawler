'use strict';

const Axios = require('axios');
const Config = require('config');

exports.seed = async function (db) {

    const config = Config.location;
    const bbox = `(${config.bounds.south}, ${config.bounds.west}, ${config.bounds.north}, ${config.bounds.east})`;

    const response = await Axios.get(`${config.api.baseUrl}${config.api.path}`, {
        params: {
            data: `[out:${config.api.output}];node[amenity=pub]${bbox};out;`
        }
    });

    const pubs = db.getSchema().getCollection('pubs');
    const data = response.data.elements
        .reduce((result, elem) => {

            if (elem.tags.amenity !== 'pub') {
                return result;
            }

            const copy = Object.assign({}, { _id: elem.id }, elem);
            delete copy.id;

            return result.concat(copy);
        }, []);

    await pubs.add(data).execute();
};
