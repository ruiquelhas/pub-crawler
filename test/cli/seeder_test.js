'use strict';

const Code = require('code');
const Config = require('config');
const Db = require('lib/db');
const Lab = require('lab');
const Nock = require('nock');
const Seeder = require('lib/cli/seeder');

const lab = exports.lab = Lab.script();

lab.experiment('seeder', () => {

    let db;

    lab.before(async () => {

        Nock.disableNetConnect();

        const config = Config.database;

        db = Db();
        db.connection(config);

        await db.reset();
        await db.start();
    });

    lab.after(async () => {

        Nock.cleanAll();
        Nock.enableNetConnect();

        await db.reset();
        await db.disconnect();
    });

    lab.test('should seed the database using data from the overpass API', async () => {

        const data = [{
            id: 'foo',
            tags: {
                amenity: 'pub'
            }
        }, {
            id: 'bar',
            tags: {
                amenity: 'somethingElse'
            }
        }, {
            id: 'baz',
            tags: {
                amenity: 'pub'
            }
        }];

        const config = Config.location;
        const bbox = `(${config.bounds.south}, ${config.bounds.west}, ${config.bounds.north}, ${config.bounds.east})`;

        const overpassAPI = Nock(config.api.baseUrl);

        overpassAPI.get(config.api.path)
            .query({ data: `[out:${config.api.output}];node[amenity=pub]${bbox};out;` })
            .reply(200, { elements: data });

        const pubs = db.getSchema().getCollection('pubs');
        const actual = [];

        await Seeder.seed(db);
        await pubs.find().execute((pub) => pub && actual.push(pub));

        Code.expect(actual).to.only.include([{
            _id: 'foo',
            tags:{
                amenity: 'pub'
            }
        }, {
            _id: 'baz',
            tags: {
                amenity: 'pub'
            }
        }]);

        overpassAPI.done();
    });
});
