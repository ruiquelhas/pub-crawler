'use strict';

const App = require('lib');
const Code = require('code');
const Config = require('config');
const Db = require('lib/db');
const Hapi = require('hapi');
const Lab = require('lab');
const Seeder = require('lib/cli/seeder');

const lab = exports.lab = Lab.script();

lab.experiment('pub-crawler', () => {

    let db;
    let server;

    lab.before(async () => {

        db = Db();
        db.connection(Config.database);

        server = new Hapi.Server();
        server.app.db = db;
        server.connection();

        await db.reset();
        await db.start();
        await server.register(App);
    });

    lab.after(async () => {

        await db.reset();
        await db.disconnect();
    });

    lab.experiment('pubs', () => {

        lab.beforeEach(async () => {

            await db.clear();
            await Seeder.seed(db);
        });

        lab.test('should retrieve the list of nearby pubs', async () => {

            const response = await server.inject('/pubs');

            Code.expect(response.statusCode).to.equal(200);
            Code.expect(response.result).to.be.an.array();
            Code.expect(response.result.length).to.be.above(0);

            response.result.forEach((location) => {

                Code.expect(location).to.include(['_id', 'lat', 'lon', 'tags', 'type']);
                Code.expect(location.tags).to.include(['amenity', 'name']);
            });
        });

        lab.test('should retrieve the list of nearby pubs that match a given criteria', async () => {

            const response = await server.inject('/pubs?tags[internet_access]&tags[food]=yes');

            Code.expect(response.statusCode).to.equal(200);
            Code.expect(response.result).to.be.an.array();
            Code.expect(response.result.length).to.be.above(0);

            response.result.forEach((location) => {

                Code.expect(location.tags.internet_access).to.exist();
                Code.expect(location.tags.food).to.equal('yes');
            });
        });

        lab.test('should provide pagination support', async () => {

            const fstPageSize = 3;
            const defaultSize = 5;
            const fstPage = await server.inject(`/pubs?page=1&size=${fstPageSize}`);
            const sndPage = await server.inject('/pubs?page=2');

            Code.expect(fstPage.statusCode).to.equal(200);
            Code.expect(fstPage.result).to.be.an.array();
            Code.expect(fstPage.result).to.have.length(fstPageSize);

            Code.expect(sndPage.statusCode).to.equal(200);
            Code.expect(sndPage.result).to.be.an.array();
            Code.expect(sndPage.result).to.have.length(defaultSize);

            Code.expect(fstPage.result).to.not.include(sndPage.result);
            Code.expect(sndPage.result).to.not.include(fstPage.result);
        });

        lab.test('should retrieve details of a given existing pub', async () => {

            const pubList = await server.inject('/pubs?size=1');
            const singlePub = await server.inject(`/pubs/${pubList.result[0]._id}`);

            Code.expect(singlePub.statusCode).to.equal(200);
            Code.expect(singlePub.result).to.be.an.object();
            Code.expect(singlePub.result).to.include(['_id', 'lat', 'lon', 'tags', 'type']);
        });

        lab.test('should fail to retrieve details of a given non-existing pub', async () => {

            const singlePub = await server.inject('/pubs/0');

            Code.expect(singlePub.statusCode).to.equal(404);
        });

        lab.test('should allow to wipe out the entire database', async () => {

            const firstTry = await server.inject('/pubs');
            Code.expect(firstTry.result).to.not.be.empty();

            const wipeOut = await server.inject({ method: 'DELETE', url: '/' });
            Code.expect(wipeOut.statusCode).to.equal(200);

            const secondTry = await server.inject('/pubs');
            Code.expect(secondTry.result).to.be.empty();
        });
    });
});
