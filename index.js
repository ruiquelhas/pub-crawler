'use strict';

const App = require('./lib');
const Config = require('config');
const Db = require('./lib/db');
const Hapi = require('hapi');
const Inert = require('inert');
const Lout = require('lout');
const Vision = require('vision');

(async function () {

    const db = Db();
    db.connection(Config.database);

    await db.start();

    const server = new Hapi.Server();
    server.app.db = db;
    server.connection(Config.server);

    await server.register([Vision, Inert, Lout, App]);
    await server.start();

    console.log('Server running at:', server.info.uri);
}());
