#!/usr/bin/env node

'use strict';

const Config = require('config');
const Db = require('../lib/db');
const Seeder = require('../lib/cli/seeder');

(async function () {

    const config = Config.database;
    const database = Db();
    database.connection(config);

    await database.start();
    await Seeder.seed(database);
    await database.disconnect();

    console.log('done');
}());
