# pub-crawler

Crawl for the best nearby places to grab some drinks.

## Running the app

If you have not already, download and install [Node.js](https://nodejs.org/en/download/current/) and [Docker](https://docs.docker.com/engine/installation/). Just make sure you also set a database password, since empty passwords are not allowed for non-root MySQL users.

```sh
$ npm i
$ export PC_DB_PASSWORD=<password>
$ npm run docker:start
```

A link should show up, click on it or navigate to [here](http://localhost:3000). The API documentation should be available [here](http://localhost:3000/docs).

## Seeding the database

```sh
$ npm run db:seed
```

## Destroying the container and cleaning up

There is no shared volume for the data so, everytime the containers are destroyed, everything is gone (great for demos!).

```sh
$ npm run docker:teardown
```
